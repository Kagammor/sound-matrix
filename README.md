# Sound Matrix #

This is an interactive matrix for building simple sound loops. Utilizing the Web Audio API, it does not depend on Flash. Matrix.js depends on Font Awesome and the jQuery and tones libraries.

```
#!javascript

/*------------
 Methods
------------*/

// Initialize sound matrix and DOM. An object with variables may be passed as an argument.
matrix.initialize(); // matrix.initialize({'beats': 8, 'notes': 8})

// Queue (add) or remove note in matrix. Expects coordinates in matrix starting from [0,0].
matrix.toggle(beat, note); // matrix.toggleNote(0,0)

// Start beat loop.
matrix.start();

// Pause beat loop.
matrix.pause();

// Stop beat loop.
matrix.stop();

// Load matrix preset from string.
matrix.load(hash); // matrix.load('12%12%240%440%0%100%1kw%e8%3k%w%g%g%1%0%0%0%0%0');

// Retrieve preset from URL hash and load it. This allows the matrix to be shared and bookmarked!
matrix.loadURL();

// Update URL hash with the current settings. The URL hash will rarely have to be updated manually.
matrix.updateURL();

// Set time signature for indicator and metronome in notes per measure.
matrix.setTime(); // 4

// Empty matrix and clear URL hash.
matrix.clear();

/*------------
 Variables
------------*/

// Variables can be called and set either directly or passed in an object as an argument for matrix.initialize().

// DOM element to initialize sound matrix in. Can only be set with matrix.initialize('grid': $('#matrix')).
matrix.grid; // $('#matrix')

// Size of matrix in beats (x-axis) and notes (y-axis). Can only be set with matrix.initialize({'beats': 8, 'notes': 8}).
matrix.beats; // 8
matrix.notes; // 8

// Array of default controls to include in order of appearance. When unspecified, all controls will be enabled. Can only be set with matrix.initialize({'controls': ['grid', 'notes', 'start', 'pause', 'stop', 'clear', 'bpm', 'time', 'metronome', 'frequency', 'waveform', 'attack', 'release']}).
matrix.controls;

// The tempo in beats per minute. Changes will require the matrix to be stopped and restarted.
matrix.bpm; // 240

// Instrument to use for sounds. Can be set to 'wave' or 'drums' or 0, 1 respectively.
matrix.instrument; // 'wave'

// Path to (.wav) files for instruments. Directory names in path must equal respective instrument name.
matrix.path; // './assets/audio/'

// Waveform of sounds. Can be set to 'sine', 'square', 'sawtooth' or 'triangle' or 0, 1, 2, 3 respectively. Only applies to 'wave' instrument.
matrix.waveform; // 0

// Frequency (Hz) of lowest note. Other notes will be calculated based on this value.
matrix.frequency; // 440

// Time signature for indicator and metronome in notes per measure. Can only be set with matrix.initialize({'time': 4}) or matrix.setTime(4).
matrix.time; // 4

// Attack of the envelope in milliseconds. The time it takes for a sound to reach full volume.
matrix.attack; // 0

// Release of the envelope in milliseconds. The time it takes for a sound to reach silence.
matrix.release; // 300

// Metronome to tick at the configured beats per minute.
matrix.metronome; // false

// Current beat.
matrix.beat; // 0

// Pauses beat loop when browser tab loses focus. See notes.
matrix.blurPause; // true

```

## Notes ##
* *blurPause*, when set to *true*, will pause the sound matrix when the browser tab loses focus. Some browsers intentionally throttle the setInterval() method to 1000ms when the browser tab it runs in loses focus, effectively slowing the sound matrix down to 60bpm. When the entire browser window loses focus, but the browser tab remains exposed, the sound matrix will not be paused.
* Certain variables require the matrix to be reinitialized, and can thus only be set with matrix.initialize(). These variables will simply be ignored when set directly.
