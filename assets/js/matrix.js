var matrix = {
	'status': 'stopped',
	'queue': {},
	'instruments': ['wave', 'drums'],
	'waves': ['sine', 'square', 'sawtooth', 'triangle'],
	'initialize': function(config) {
		config = config || {};
	
		matrix['grid'] = config.grid || $('#matrix');
		matrix['beats'] = config.beats || 8;
		matrix['notes'] = config.notes || 8;
		matrix['controls'] = (Array.isArray(config.controls) || false) ? config.controls : ['notes', 'grid', 'start', 'pause', 'stop', 'clear', 'bpm', 'time', 'metronome', 'instrument', 'waveform', 'frequency', 'attack', 'release'];
		matrix['bpm'] = config.bpm || 240;
		matrix['time'] = config.time || 4;
		matrix['instrument'] = config.instrument || matrix.instruments[(config.instrument || -1)] ? config.instrument : (matrix.instruments.indexOf((config.instrument || false)) > -1 ? matrix.instruments.indexOf(config.instrument) : 0); // Translate instrument string to integer if necessary
		matrix['path'] = config.path || './assets/audio/';
		matrix['waveform'] = matrix.waves[(config.waveform || -1)] ? config.waveform : (matrix.waves.indexOf((config.waveform || false)) > -1 ? matrix.waves.indexOf(config.waveform) : 0); // Translate waveform string to integer if necessary
		matrix['frequency'] = config.frequency || 440;
		matrix['attack'] = config.attack || 0;
		matrix['release'] = config.release || 100;
		matrix['metronome'] = config.metronome || false;
		matrix['blurPause'] = config.blurPause || true;

		matrix.grid.empty();
		matrix.queue = {};
	
		for(beatCount=0;beatCount<matrix.beats;beatCount++) {
			var tileSize = matrix.grid.width() / matrix.beats;
			
			var beat = $('<ul class="matrixBeat" id="matrixBeat' + beatCount + '"  style="width: ' + tileSize + 'px;">');
			matrix.queue[beatCount] = {};

			for(noteCount=0;noteCount<matrix.notes;noteCount++) {
				var note = $('<li class="matrixNote" id="matrixNote' + beatCount + ':' + noteCount + '" style="width: ' + tileSize + 'px; height: ' + tileSize + 'px;">');
				beat.append(note);

				matrix.queue[beatCount][noteCount] = false;
			}
			
			beat.prepend($('<li class="matrixIndicator"' + (beatCount % matrix.time === 0 ? ' style="border-top: none;""' : '') + '>'));
			matrix.grid.append(beat);
		}
		
		matrix.setControls(matrix.controls);
		
		return true;
	},
	'toggle': function(beat, note) {
		if(matrix.queue[beat][note] === false) {
			matrix.queue[beat][note] = true;
			$('#matrixNote' + beat + '\\:' + note).addClass('matrixNoteQueued');
	
			matrix.updateURL();
			
			return true;
		} else {
			matrix.queue[beat][note] = false;
			$('#matrixNote' + beat + '\\:' + note).removeClass('matrixNoteQueued');
	
			matrix.updateURL();
			
			return false;
		}
	},
	'interval': 0,
	'beat': 0,
	'start': function() {
		if(matrix.source === 'oscillator') {
			var audioContext = new AudioContext();
		}

		matrix.beat = matrix.status === 'paused' ? matrix.beat : 0;
		
		var pulse = function() {
			if(matrix.status === 'started') {
				if(matrix.beat >= matrix.beats) {
					matrix.beat = 0;
				}
			
				$('.matrixBeat').removeClass('matrixBeatPlayed');
				$('#matrixBeat' + matrix.beat).addClass('matrixBeatPlayed');

				for(var queued in matrix.queue[matrix.beat]) {
					if(matrix.queue[matrix.beat][queued]) {
						if(matrix.instrument === 0) {
							tones.type = matrix.waves[matrix.waveform];
							tones.attack = matrix.attack === 0 ? 1 : matrix.attack;
							tones.release = matrix.release;
							
							tones.play(matrix.frequency * Math.pow(1.059463, matrix.notes - queued));
						} else {
							new Audio(matrix.path + matrix.instruments[matrix.instrument] + '/' + (matrix.notes - queued) + '.wav').play();
						}
					}
				}
				
				if(matrix.metronome) {
					tones.type = 'triangle';
					tones.attack = 0;
						
					if(matrix.beat % matrix.time === 0) {
						tones.release = 20;
					} else {
						tones.release = 5;
					}
					
					tones.play(480);
				}

				matrix.beat++;
			}
		}

		if(matrix.status === 'stopped') {
			matrix.interval = setInterval(function() {
				if(matrix.blurPause) {
					window.requestAnimationFrame(function() {
						pulse();
					});
				} else {
					pulse();
				}
			}, matrix.bpm > 0 ? 60000 / matrix.bpm : 86400000); // Prevent division by 0, interpret as 1 beat per 24 hours
		}
		
		matrix.status = 'started';
		
		return true;
	},
	'stop': function() {
		clearInterval(matrix.interval);
		$('.matrixBeat').removeClass('matrixBeatPlayed');

		matrix.beat = 0;
		matrix.status = 'stopped';
		
		return true;
	},
	'pause': function() {
		if(matrix.status === 'started') {
			matrix.status = 'paused';
			
			return true;
		} else {
			return false;
		}
	},
	'clear': function() {
		matrix.initialize({'grid': matrix.grid, 'beats': matrix.beats, 'notes': matrix.notes, 'controls': matrix.controls});
		matrix.stop();
		
		window.location.hash = '';
		
		return true;
	},
	'load': function(hash) {
		hash = hash.replace('#', '').split(':');
	
		if(hash[0].length > 0) {
			matrix.beats = parseInt(hash.splice(0, 1)[0]);
			matrix.notes = parseInt(hash.splice(0, 1)[0]);
			
			matrix.initialize({'grid': matrix.grid, 'beats': matrix.beats, 'notes': matrix.notes, 'time': matrix.time, 'controls': matrix.controls});
			
			matrix.bpm = parseInt(hash.splice(0, 1)[0]);
			$('#matrixBPM').val(matrix.bpm);
					
			matrix.time = parseInt(hash.splice(0, 1)[0]);
			matrix.setTime(matrix.time);
			$('#matrixTime').val(matrix.time);
			
			matrix.instrument = parseInt(hash.splice(0, 1)[0]);
			$('#matrixInstrument').val(matrix.instrument);
			
			matrix.waveform = parseInt(hash.splice(0, 1)[0]);
			$('#matrixWaveform').val(matrix.waveform);
			
			matrix.frequency = parseInt(hash.splice(0, 1)[0]);
			$('#matrixFrequency').val(matrix.frequency);
			
			matrix.attack = parseInt(hash.splice(0, 1)[0]);
			$('#matrixAttack').val(matrix.attack);
			
			matrix.release = parseInt(hash.splice(0, 1)[0]);
			$('#matrixRelease').val(matrix.release);

			for(var beat in hash) {
				queue = parseInt(hash[beat], 36).toString(2);
				
				while(queue.length < matrix.notes) {
					queue = '0' + queue;
				}
			
				for(var note in queue) {
					if(parseInt(queue[note]) === 1) {
						matrix.toggle(beat, note);
					}
				}
			}
			
			return true;
		} else {
			return false;
		}
	},
	'loadURL': function() {
		matrix.load(window.location.hash);
	},
	'updateURL': function() {
		var link = [matrix.beats, matrix.notes, matrix.bpm, matrix.time, matrix.instrument, matrix.waveform, matrix.frequency, matrix.attack, matrix.release];

		for(var beat in matrix.queue) {
			var queue = '';

			for(var note in matrix.queue[beat]) {
				queue = queue + (matrix.queue[beat][note] ? 1 : 0);
			}
	
			link.push(parseInt(queue, 2).toString(36));
		}

		window.location.hash = link.join(':');
		
		return true;
	},
	'setTime': function(time) {
		matrix.time = time;
		
		console.log(matrix.time);
		
		$('.matrixIndicator').css('border', '');
		
		$('.matrixIndicator').each(function(i) {
			if(i % matrix.time === 0) {
				$(this).css('border-top', 'none');
			}
		});
	},
	'setControls': function(controls) {
		var controlPanel = $('<div class="controlPanel">');
		
		var controlPanelRowA = $('<div class="controlPanelRow">');
		var controlPanelRowB = $('<div class="controlPanelRow">');
		var controlPanelRowC = $('<div class="controlPanelRow">');
		
		controlPanel.append(controlPanelRowB);
		controlPanel.append(controlPanelRowC);
		controlPanel.append(controlPanelRowA);
		
		matrix.grid.append(controlPanel);
	
		for(var control in controls) {
			switch(controls[control]) {
				case 'notes':
					$('.matrixNote').click(function() {
						matrix.toggle(this.id.replace('matrixNote', '').split(':')[0], this.id.split(':')[1]);
					});
	
					var mouseDown = false;
					var touched = true;

					$(document).mousedown(function() {
						mouseDown = true;
					}).mouseup(function() {
						mouseDown = false;  
					});

					$('.matrixNote').mouseenter(function() {
						if(mouseDown === true) {
							matrix.toggle(this.id.replace('matrixNote', '').split(':')[0], this.id.split(':')[1]);
						}
					});
					
					break;
				case 'start':
					controlPanelRowA.append($('<div class="inputContainer"><button id="matrixStart"><i class="fa fa-play"></i></button></div>'));
						
					$('#matrixStart').click(function() {
						matrix.start();
					});
					
					break;
				case 'pause':
					controlPanelRowA.append($('<div class="inputContainer"><button id="matrixPause"><i class="fa fa-pause"></i></button></div>'));
				
					$('#matrixPause').click(function() {
						matrix.pause();
					});
					
					break;
				case 'stop':
					controlPanelRowA.append($('<div class="inputContainer"><button id="matrixStop"><i class="fa fa-stop"></i></button></div>'));
					
					$('#matrixStop').click(function() {
						matrix.stop();
					});
					
					break;
				case 'clear':
					controlPanelRowA.append($('<div class="inputContainer"><button id="matrixClear"><i class="fa fa-trash"></i></button></div>'));
					
					$('#matrixClear').click(function() {
						matrix.clear();
					});
					
					break;
				case 'instrument':
					controlPanelRowB.append($('<div class="inputContainer"><i class="fa fa-music"></i><select id="matrixInstrument"><option value="0"' + (matrix.instrument === 'wave' ? 'selected="selected"' : '') + '>wave</option><option value="1"' + (matrix.instrument === 'drums' ? 'selected="selected"' : '') + '>drums</option></select></div>'));

					$('#matrixInstrument').change(function() {
						matrix.instrument = $('#matrixInstrument').val();
						matrix.updateURL();
					});
				
					break;
				case 'waveform':
					controlPanelRowB.append($('<div class="inputContainer"><i class="fa fa-area-chart"></i><select id="matrixWaveform"><option value="0"' + (matrix.waveform === 0 ? 'selected="selected"' : '') + '>sine</option><option value="1"' + (matrix.waveform === 1 ? 'selected="selected"' : '') + '>square</option><option value="2"' + (matrix.waveform === 2 ? 'selected="selected"' : '') + '>sawtooth</option><option value="3"' + (matrix.waveform === 3 ? 'selected="selected"' : '') + '>triangle</option></select></div>'));

					$('#matrixWaveform').change(function() {
						matrix.waveform = parseInt($('#matrixWaveform').val());
						matrix.updateURL();
					});
				
					break;
				case 'frequency':
					controlPanelRowB.append($('<div class="inputContainer"><i class="fa fa-tachometer"></i><input type="number" id="matrixFrequency" value="' + matrix.frequency + '"></div>'));

					$('#matrixFrequency').change(function() {
						matrix.frequency = parseInt($('#matrixFrequency').val());
						matrix.updateURL();
					});
				
					break;
				case 'attack':
					controlPanelRowB.append($('<div class="inputContainer"><i class="fa fa-signal"></i><input type="number" id="matrixAttack" value="' + matrix.attack + '"></div>'));

					$('#matrixAttack').change(function() {
						matrix.attack = parseInt($('#matrixAttack').val());
						matrix.updateURL();
					});
				
					break;
				case 'release':
					controlPanelRowB.append($('<div class="inputContainer"><i class="fa fa-signal fa-flip-horizontal"></i><input type="number" id="matrixRelease" value="' + matrix.release + '"></div>'));

					$('#matrixRelease').change(function() {
						matrix.release = parseInt($('#matrixRelease').val());
						matrix.updateURL();
					});
				
					break;
				case 'grid':
					controlPanelRowC.append($('<div class="inputContainer"><i class="fa fa-table"></i><input type="number" id="matrixGridBeats" class="matrixGridInput" max="64" value="' + matrix.beats + '">x<input type="number" id="matrixGridNotes" class="matrixGridInput" max="64" value="' + matrix.notes + '"></div>'));

					$('#matrixGridBeats').change(function() {
						matrix.beats = $('#matrixGridBeats').val() <= 64 ? parseInt($('#matrixGridBeats').val()) : matrix.beats;
						matrix.initialize({'grid': matrix.grid, 'beats': matrix.beats, 'notes': matrix.notes, 'controls': matrix.controls});
					
						matrix.updateURL();
					});
					$('#matrixGridNotes').change(function() {
						matrix.notes = $('#matrixGridNotes').val() <= 64 ? parseInt($('#matrixGridNotes').val()) : matrix.beats;
						matrix.initialize({'grid': matrix.grid, 'beats': matrix.beats, 'notes': matrix.notes, 'controls': matrix.controls});
					
						matrix.updateURL();
					});
				
					break;
				case 'bpm':
					controlPanelRowC.append($('<div class="inputContainer"><i class="fa fa-heartbeat"></i><input type="number" id="matrixBPM" value="' + matrix.bpm + '"></div>'));
					
					$('#matrixBPM').change(function() {
						matrix.bpm = parseInt($('#matrixBPM').val());
		
						if(matrix.status !== 'stopped') {
							matrix.stop();
							matrix.start();
						}
		
						matrix.updateURL();
					});
					
					break;
				case 'time':
					controlPanelRowC.append($('<div class="inputContainer"><i class="fa fa-clock-o"></i><select id="matrixTime"><option value="2"' + (matrix.time === 2 ? 'selected="selected"' : '') + '>2/4</option><option value="3"' + (matrix.time === 3 ? 'selected="selected"' : '') + '>3/4</option><option value="4"' + (matrix.time === 4 ? 'selected="selected"' : '') + '>4/4</option></select></div>'));

					$('#matrixTime').change(function() {
						matrix.time = parseInt($('#matrixTime').val());
						matrix.setTime(matrix.time);
						
						matrix.updateURL();
					});
				
					break;
				case 'metronome':
					controlPanelRowC.append($('<div class="inputContainer"><i class="fa fa-external-link"></i><input type="checkbox" id="matrixMetronome"></div>'));

					$('#matrixMetronome').change(function() {
						matrix.metronome = $('#matrixMetronome').prop('checked');
					});
				
					break;
			}
		}
		
		return true;
	}
}
